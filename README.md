# Flutterwave Rave (Drupal 8 Module)

> Implement Flutterwave Rave payment gateway easily with Drupal 8

Go to [Flutterwave Rave](https://ravepay.co) to get your public and private key

## Installation
[Install](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules) as a regular Drupal Module.

More documentation coming soon. 

>Test Card

```bash
5438898014560229
cvv 789
Expiry Month 09
Expiry Year 19
Pin 3310
otp 12345
```

>Test Bank Account

```bash
Access Bank
Account number: 0690000031
otp: 12345
```
For [More Test Cards](https://flutterwavedevelopers.readme.io/docs/test-cards)
For [More Test Bank Accounts](https://flutterwavedevelopers.readme.io/docs/test-bank-accounts)

## Credits

- [Elijah Oyekunle](https://github.com/playmice)

## Contributing
Please feel free to fork this package and contribute by submitting a pull request to enhance the functionalities.


Kindly star the GitHub repo and share ❤️.  I ❤️ Flutterwave

Kindly [follow me on twitter](https://twitter.com/elijahoyekunle)!


## License

Please see [License File](LICENSE.txt) for more information.
